package conditionalExecution.accountCase;

public class Account {

    private String accountId;
    private double balance;

    public Account(String accountId, double balance) {
        this.accountId = accountId;
        this.balance = balance;
    }

    public Account(String accountId) {
        this(accountId, 0.0);
    }

    public double checkBalance() {
        return balance;
    }

    public void save(double saveBalance) {

        this.balance += saveBalance;
        System.out.println("Saving account balance: " + this.balance);
    }

    public String getAccountId() {
        return accountId;
    }

    public void withdraw(double amount) {

        String withdrawalMessage = "Your balance is insufficient";

        if(this.balance >= amount){
            this.balance -= amount;
            withdrawalMessage = "Successfully withdraw " + amount + " from the account";
        }

        System.out.println(withdrawalMessage);
    }

}
