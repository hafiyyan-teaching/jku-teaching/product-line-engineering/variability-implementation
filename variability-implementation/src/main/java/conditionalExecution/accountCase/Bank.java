package conditionalExecution.accountCase;

import java.util.HashMap;
import java.util.Map;

public class Bank {

    private Map<String, Account> accounts;
    private String bankName;
    private boolean overdraftOption;
    private final double OVERDRAFT_AMOUNT = 10.0;

    public Bank(Map<String, Account> accounts, String bankName, boolean overdraftOption) {
        this.accounts = accounts;
        this.bankName = bankName;
        this.overdraftOption = overdraftOption;
    }

    public Bank(String bankName) {
        this(new HashMap<String, Account>(), bankName, false);
    }

    public void registerAccount(String accountId) {
        registerAccount(accountId, 0.0);
    }

    public void registerAccount(String accountId, double balance) {
        accounts.put(accountId, new Account(accountId, balance));
        System.out.println("Account registered with ID: " + accountId);
    }

    public double checkBalance(String accountId) {
        return accounts.get(accountId).checkBalance();
    }

    public void save(String accountId, double saveBalance) {

        Account selectedAccount = accounts.get(accountId);

        if(!overdraftOption){
            selectedAccount.save(saveBalance);
        } else {
            if(saveBalance > OVERDRAFT_AMOUNT){
                selectedAccount.save(saveBalance - OVERDRAFT_AMOUNT);
            } else {
                System.out.println("Save balance is not enough to cover the overdraft expense");
            }
        }

    }

    public void withdraw(String accountId, double amount) {

        Account selectedAccount = accounts.get(accountId);

        if(!overdraftOption) {
            selectedAccount.withdraw(amount);
        } else {
            if(selectedAccount.checkBalance() > (OVERDRAFT_AMOUNT + amount)){
                selectedAccount.save(amount + OVERDRAFT_AMOUNT);
            } else {
                System.out.println("Your balance is not enough to cover " +
                        "the overdraft expense and the withdrawal amount");
            }
        }

    }
}
