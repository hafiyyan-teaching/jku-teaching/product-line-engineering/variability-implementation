package designPattern.strategy.weapon;

public interface Weapon {

    void attack();
    void parry();
    void block();

}
