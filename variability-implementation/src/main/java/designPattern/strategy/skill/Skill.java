package designPattern.strategy.skill;

public interface Skill {

    void perform();

}
