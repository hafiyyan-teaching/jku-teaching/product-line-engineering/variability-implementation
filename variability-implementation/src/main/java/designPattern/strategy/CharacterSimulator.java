package designPattern.strategy;

import designPattern.strategy.character.*;
import designPattern.strategy.character.Character;
import designPattern.strategy.guild.*;
import designPattern.strategy.weapon.*;
import designPattern.strategy.item.*;
import designPattern.strategy.skill.*;

public class CharacterSimulator {

    public static void main(String[] args){

        GuildNPC unicorn = new SpearmanNPC("Unicorn");
        GuildNPC bamba = new WarriorNPC("Bamba");

        Character andy = new Spearman("Andy", unicorn);
        Character kirito = new Knight("Kirigaya Kazuto", bamba);

        Weapon sword = new Sword("Excalibur");
        Weapon twinSwords = new TwinSwords("Agni and Rudra");
        Weapon spear = new Spear("Sky Piercer");
        Weapon bow = new Bow("Vijaya");

        Item healthPotion = new Potion("Health Potion");
        Item manaPotion = new Potion("Mana Potion");
        Item teleportationScroll = new Scroll("Teleportation Scroll");
        Item attackPill = new BufferPill("Attack Pill");
        Item defensePill =  new BufferPill("Defense Pill");

        Skill fly = new Fly();
        Skill fireMagic = new Magic("Fire Magic");
        Skill waterMagic = new Magic("Water Magic");
        Skill windMagic = new Magic("Wind Magic");
        Skill running = new Running();

        andy.setSkill(fireMagic);
        andy.setWeapon(spear);
        andy.addInventory(healthPotion);
        andy.addInventory(manaPotion);
        andy.addInventory(defensePill);

        kirito.setSkill(windMagic);
        kirito.setWeapon(twinSwords);
        kirito.addInventory(healthPotion);
        kirito.addInventory(manaPotion);
        kirito.addInventory(attackPill);

        kirito.useInventory();
        kirito.attackCharacter(andy);

        bamba.setBroadcastMessage("New Sword has been announced. Please check special quest menu");
        unicorn.setBroadcastMessage("New Spear has been announced. Please check special quest menu");

    }

}
