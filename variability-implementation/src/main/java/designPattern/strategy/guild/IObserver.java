package designPattern.strategy.guild;

public interface IObserver {
    void update();
}
