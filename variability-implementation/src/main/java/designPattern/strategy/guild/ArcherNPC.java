package designPattern.strategy.guild;

public class ArcherNPC extends GuildNPC {
    public ArcherNPC(String name) {
        super(name);
    }

    @Override
    public void setBroadcastMessage(String message) {
        this.bcMessage = message;
        this.notifyObserver();
    }
}
