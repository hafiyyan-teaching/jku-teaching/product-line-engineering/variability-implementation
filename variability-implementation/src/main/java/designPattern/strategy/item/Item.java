package designPattern.strategy.item;

public interface Item {

    void use();
    void fix();

}
