package designPattern.ioc.application;

import designPattern.ioc.emailService.MailService;
import javax.inject.Inject;

public class CloudApplication {

    private static MailService mailService;

    @Inject
    public CloudApplication(MailService mailService) {

        this.mailService = mailService;
    }

    public boolean sendMessage(String subject, String body, String emailTarget){

        try{
            mailService.send(emailTarget, subject, body);
        } catch (Exception e){
            return false;
        }

        return true;
    }
}
