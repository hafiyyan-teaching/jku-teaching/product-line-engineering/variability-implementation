package designPattern.ioc.application;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class DemonstrationClass {

    public static void main(String[] args) {

        Injector injector = Guice.createInjector(new ApplicationConfigurator());
        CloudApplication cloudApplication = injector.getInstance(CloudApplication.class);

        cloudApplication.sendMessage("Buy Gunpla",
                "I want to buy Gundam Plastic Model", "order@gunpla.com");

    }

}
