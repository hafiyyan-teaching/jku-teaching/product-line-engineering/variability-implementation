package designPattern.ioc.application;

import com.google.inject.AbstractModule;
import designPattern.ioc.emailService.ICloudMail;
import designPattern.ioc.emailService.MailService;


public class ApplicationConfigurator extends AbstractModule {

    @Override
    protected void configure() {

        //bind the service to implementation class
        bind(MailService.class).to(ICloudMail.class);

    }

}
