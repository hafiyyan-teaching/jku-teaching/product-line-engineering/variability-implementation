package designPattern.ioc.emailService;

import javax.inject.Singleton;

@Singleton
public class GoogleMail implements MailService {


    @Override
    public void send(String to, String subject, String body) {

        System.out.println("Google Mail Client, ID: 1991");
        System.out.println("Sending email to " + to);
        System.out.println("Subject: " + subject);
        System.out.println("Body: " + body);

    }
}
