package designPattern.ioc.emailService;

import javax.inject.Singleton;

@Singleton
public class OutlookMail implements MailService {

    @Override
    public void send(String to, String subject, String body) {

        System.out.println("######Client ID 3245######");
        System.out.println("To: " + to);
        System.out.println("Subject: " + subject);
        System.out.println("Body: " + body);
        System.out.println("######Outlook Mail######");

    }
}
