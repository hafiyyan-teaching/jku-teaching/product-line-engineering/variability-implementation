package designPattern.ioc.emailService;

import javax.inject.Singleton;

@Singleton
public class YahooMail implements MailService {

    @Override
    public void send(String to, String subject, String body) {

        System.out.println("To ##" + to + "##");
        System.out.println("Subject: " + subject);
        System.out.println("Body: " + body);
        System.out.println("#################");
        System.out.println("Sent from: Yahoo Mail Client with ID: 9119");
    }
}
