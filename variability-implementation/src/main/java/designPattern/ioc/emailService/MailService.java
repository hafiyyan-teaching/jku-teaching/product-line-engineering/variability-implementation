package designPattern.ioc.emailService;

public interface MailService {

    public void send(String to, String subject, String body);

}
