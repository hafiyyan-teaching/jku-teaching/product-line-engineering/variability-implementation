package designPattern.ioc.emailService;

import javax.inject.Singleton;

@Singleton
public class ICloudMail implements MailService{

    @Override
    public void send(String to, String subject, String body) {

        System.out.println("iCloud Mail Client 9191");
        System.out.println("To: " + to);
        System.out.println("Subject: " + subject);
        System.out.println("Body: " + body);
        System.out.println("#########################");
    }
}
