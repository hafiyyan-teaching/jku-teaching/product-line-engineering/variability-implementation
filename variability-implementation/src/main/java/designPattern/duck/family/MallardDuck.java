package designPattern.duck.family;

import designPattern.duck.fly.FlyWithWings;
import designPattern.duck.quack.Quack;

public class MallardDuck extends Duck {

    public MallardDuck(String name) {

        setFlyBehavior(new FlyWithWings());
        setQuackBehavior(new Quack());
        setName(name);
    }

    @Override
    public void display() {
        System.out.println("I am a mallard duck named: " + this.getName());
    }
}
