package designPattern.duck.family;

import designPattern.duck.fly.NoFlying;
import designPattern.duck.quack.Mute;

public class RubberDuck extends Duck {

    public RubberDuck(final String name) {

        setFlyBehavior(new NoFlying());
        setQuackBehavior(new Mute());
        setName(name);

    }

    @Override
    public void display() {
        System.out.println("I am a rubber duck named: " + this.getName());
    }

}
