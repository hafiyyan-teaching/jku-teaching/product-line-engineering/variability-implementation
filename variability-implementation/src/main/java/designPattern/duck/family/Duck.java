package designPattern.duck.family;

import designPattern.duck.fly.FlyBehavior;
import designPattern.duck.quack.QuackBehavior;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;
    private String name;

    public abstract void display();

    protected void setFlyBehavior(final FlyBehavior flyBehavior){

        this.flyBehavior = flyBehavior;

    }

    protected void setName(final String name){

        this.name = name;

    }

    protected void setQuackBehavior(final QuackBehavior quackBehavior){

        this.quackBehavior = quackBehavior;

    }

    public void fly(){

        this.flyBehavior.fly();

    }

    public void quack(){

        this.quackBehavior.quack();

    }

    public String getName() {
        return this.name;
    }
}
