package designPattern.duck.family;

import designPattern.duck.fly.FlyWithWings;
import designPattern.duck.quack.Squeak;

public class RedheadDuck extends Duck{

    public RedheadDuck(final String name) {

        setFlyBehavior(new FlyWithWings());
        setQuackBehavior(new Squeak());
        setName(name);

    }

    @Override
    public void display() {
        System.out.println("I am a redhead duck named: " + this.getName());
    }

}
