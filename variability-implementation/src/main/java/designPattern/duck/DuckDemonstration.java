package designPattern.duck;

import designPattern.duck.family.MallardDuck;
import designPattern.duck.family.RedheadDuck;
import designPattern.duck.family.RubberDuck;

public class DuckDemonstration {

    public static void main(String[] args) {

        MallardDuck mallardDuck = new MallardDuck("David");
        RedheadDuck redheadDuck = new RedheadDuck("Linda");
        RubberDuck rubberDuck = new RubberDuck("Bob");

        mallardDuck.display();
        mallardDuck.fly();

        redheadDuck.display();
        redheadDuck.quack();

        rubberDuck.display();
        rubberDuck.fly();


    }

}
