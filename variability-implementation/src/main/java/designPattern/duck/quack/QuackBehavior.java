package designPattern.duck.quack;

public interface QuackBehavior {

    void quack();

}
