package designPattern.duck.quack;

public class Mute implements QuackBehavior {

    @Override
    public void quack() {

        System.out.println("Mute quack");

    }
}
