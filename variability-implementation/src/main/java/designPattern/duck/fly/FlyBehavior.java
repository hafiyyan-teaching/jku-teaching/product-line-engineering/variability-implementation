package designPattern.duck.fly;

public interface FlyBehavior {

    void fly();

}
