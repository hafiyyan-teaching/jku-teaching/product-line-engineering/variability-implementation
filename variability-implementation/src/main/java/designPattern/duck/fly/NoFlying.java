package designPattern.duck.fly;

public class NoFlying implements FlyBehavior {

    public void fly() {
        System.out.println("I'm unable to fly!");
    }

}
